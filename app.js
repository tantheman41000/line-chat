const express = require("express");
const bodyParser = require("body-parser");
const request = require("request");
const app = express();
const axios = require("axios");

const port = process.env.PORT || 4000;

var options = {
  host: "https://private-01a9d-watcharaphanpongpan.apiary-mock.com",
  path: "/order",
  method: "GET"
};

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post("/webhook", (req, res) => {
  let reply_token = req.body.events[0].replyToken;
  let reply_text = req.body.events[0].message.text;
  console.log("๊User Token  : ", req.body.events[0].source.userId);
  console.log("message : ", reply_text);
  if (reply_text == "show order") {
    multicast(reply_token);
  } else {
    wrongReply(reply_token);
  }
  res.sendStatus(200);
});
app.listen(port);
function reply(reply_token) {
  let headers = {
    "Content-Type": "application/json",
    Authorization:
      "Bearer {BhoWp5CgwBDuUagu3tcrfcOsl3l2lrXAHuFjVqF3ZdKechOoKpwoFPFhI2Q5qnmGBGA5pCm2PWplyOhLPrO5TfIAPSCBCTQzZBwx8F5TriwLjBj7kpWDydxku0kbwA41FUyK8ElQdX6U0Y3Bd1QStQdB04t89/1O/w1cDnyilFU=}"
  };
  // var request = require("request");
  // request(
  //   "https://private-01a9d-watcharaphanpongpan.apiary-mock.com/order",
  //   function(error, response, body) {
  //     if (!error && response.statusCode == 200) {
  //       var info = JSON.parse(body);
  //     }
  //   }
  // );
  let reply_message = null;
  axios
    .get("https://private-01a9d-watcharaphanpongpan.apiary-mock.com/order")
    .then(res => {
      console.log("### data object ###", res.data.response[0].orderId);
      console.log("### data object ###", res.data.response[1].orderId);
      reply_message = res.data;
      reply_Id = res.data.response[0].orderId;
      let body = JSON.stringify({
        to: [`U18569c248e20aff74a0d7f180e4676a0`],
        replyToken: reply_token,

        //info: request,
        messages: [
          {
            type: "text",
            text: "You have press an order"
          },
          {
            type: "text",
            text: "Order ID is " + reply_Id
          }
        ]
      });
      request.post(
        {
          url: "https://api.line.me/v2/bot/message/reply",
          headers: headers,
          body: body
        },
        (err, res, body) => {
          console.log("status = " + res.statusCode);
        }
      );
    });
}

function wrongReply(reply_token) {
  let headers = {
    "Content-Type": "application/json",
    Authorization:
      "Bearer {BhoWp5CgwBDuUagu3tcrfcOsl3l2lrXAHuFjVqF3ZdKechOoKpwoFPFhI2Q5qnmGBGA5pCm2PWplyOhLPrO5TfIAPSCBCTQzZBwx8F5TriwLjBj7kpWDydxku0kbwA41FUyK8ElQdX6U0Y3Bd1QStQdB04t89/1O/w1cDnyilFU=}"
  };
  let body = JSON.stringify({
    replyToken: reply_token,
    messages: [
      {
        type: "text",
        text: "I don't understand"
      },
      {
        type: "text",
        text: "Please try again"
      }
    ]
  });
  request.post(
    {
      url: "https://api.line.me/v2/bot/message/reply",
      headers: headers,
      body: body
    },
    (err, res, body) => {
      console.log("status = " + res.statusCode);
    }
  );
}

function multicast(reply_token) {
  let headers = {
    "Content-Type": "application/json",
    Authorization:
      "Bearer {BhoWp5CgwBDuUagu3tcrfcOsl3l2lrXAHuFjVqF3ZdKechOoKpwoFPFhI2Q5qnmGBGA5pCm2PWplyOhLPrO5TfIAPSCBCTQzZBwx8F5TriwLjBj7kpWDydxku0kbwA41FUyK8ElQdX6U0Y3Bd1QStQdB04t89/1O/w1cDnyilFU=}"
  };
  let body = JSON.stringify({
    replyToken: reply_token,
    to: "U8c3242db4bc22d16185d23205cfc4e5f",
    messages: [
      {
        type: "text",
        text: "from multicast"
      },
      {
        type: "text",
        text: "you are in the right place"
      }
    ]
  });
  request.post(
    {
      url: "https://api.line.me/v2/bot/message/push",
      headers: headers,
      body: body
    },
    (err, res, body) => {
      console.log("status = " + res.statusCode);
    }
  );
}
